package com.example.al.inflater;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

	String[] name = { "Иван", "Марья", "Петр", "Антон", "Даша", "Борис", "Игорь" };
	String[] position = { "Программер", "Бухгалтер", "Программер", "Программер", "Бухгалтер", "Директор", "Охранник" };
	int salary[] = { 13000, 10000, 13000, 13000, 10000, 15000, 13000, 8000 };
	int[] colors = new int[2];

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		LayoutInflater li = getLayoutInflater();
		li.inflate(R.layout.activity_text, (LinearLayout)findViewById(R.id.linearLayout1), true);

		colors[0] = Color.parseColor("#559966FF");
		colors[1] = Color.parseColor("#553366FF");

		LinearLayout linLayout = (LinearLayout)findViewById(R.id.linLayout);

		for (int i = 0; i < name.length; i++) {
//			Log.d("myLogs", "i = " + i);
			View item = li.inflate(R.layout.item, linLayout, false);
			((TextView)item.findViewById(R.id.tvName)).setText(name[i]);
			((TextView)item.findViewById(R.id.tvPosition)).setText("Должность: " + position[i]);
			((TextView)item.findViewById(R.id.tvSalary)).setText("Оклад: " + String.valueOf(salary[i]));
			item.getLayoutParams().width = LinearLayout.LayoutParams.MATCH_PARENT;
			item.setBackgroundColor(colors[i % 2]);
			linLayout.addView(item);
		}
	}
}
